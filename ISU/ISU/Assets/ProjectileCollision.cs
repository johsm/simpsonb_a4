﻿using UnityEngine;
using System.Collections;

public class ProjectileCollision : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void OnCollisionEnter(Collision collision)
    {
        GameObject prefab = Resources.Load("fireballexplosion") as GameObject;
        GameObject fire = Instantiate(prefab) as GameObject;
        fire.transform.position = transform.position;

        Destroy(fire, 4);
        Destroy(gameObject);

    }
}
