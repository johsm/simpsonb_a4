﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

	public GameObject text;
    public GameObject player;
    private Character character;

	// Update is called once per frame
	void Update () {

        character = player.GetComponent<Character>();

        GetComponent<Image>().fillAmount = FillAmount();

		if (GetComponent<Image>().fillAmount < 0.4)
		{
			GetComponent<Image>().color = Color.red;
			text.GetComponent<Text>().color = Color.red;
		}
		else if (GetComponent<Image>().fillAmount < 0.6)
		{
			GetComponent<Image>().color = Color.yellow;
			text.GetComponent<Text>().color = Color.yellow;
		}
		else
		{
			GetComponent<Image>().color = Color.white;
			text.GetComponent<Text>().color = Color.white;
		}
	
	}

    public float FillAmount()
    {
        return (float)(character.health / character.maxHealth);
    }
}
