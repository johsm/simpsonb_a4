﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ManaBar : MonoBehaviour {

    public GameObject text;
    public GameObject player;
    private Character character;

    // Update is called once per frame
    void Update()
    {

        character = player.GetComponent<Character>();

        GetComponent<Image>().fillAmount = FillAmount();
        GetComponent<Image>().color = Color.blue;
        text.GetComponent<Text>().color = Color.white;

    }

    public float FillAmount()
    {
        return (float)(character.mana / character.maxMana);
    }
}
