﻿using UnityEngine;
using System.Collections;

public class TradeView : MonoBehaviour {

	public GameObject buyPanel;
	public GameObject sellPanel;

	public byte tradeState;
	private const byte BUY = 0;
	private const byte SELL = 1;

	// Use this for initialization
	void Start () {

		tradeState = 0;
	
	}
	
	// Update is called once per frame
	void Update () {

		if (tradeState == BUY)
		{
			buyPanel.active = true;
			sellPanel.active = false;
		}
		else if (tradeState == SELL)
		{
			buyPanel.active = false;
			sellPanel.active = true;
		}


	
	}
}
