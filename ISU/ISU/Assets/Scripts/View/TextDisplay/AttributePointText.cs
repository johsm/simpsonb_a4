﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using UnityEngine.UI;

public class AttributePointText : MonoBehaviour {

    public Text dexText;
    public Text strText;
    public Text intText;

    public Text pointsToSpendText;

    public Text healthText;
    public Text manaText;
    public Text levelText;

    public Text healthRegenText;
    public Text manaRegenText;

    public Text experienceText;

    public GameObject player;
    public Character character;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {

        pointsToSpendText.text = "Points Available: " + character.attributePointsToSpend;

        dexText.text = "Dex: " + character.dexterity;
        strText.text = "Str: " + character.strength;
        intText.text = "Int: " + character.intelligence;

        healthText.text = "Health: " + character.health + " / " + character.maxHealth;
        manaText.text = "Mana: " + character.mana + " / " + character.maxMana;
        levelText.text = "Level: " + character.level;

        healthRegenText.text = "Health Regeneration: " + character.healthRegen + " / sec";
        manaRegenText.text = "Mana Regeneration: " + character.manaRegen + " / sec";

        experienceText.text = "EXP: " + character.experience + " / " + character.NextLevelExperience(character.level); 

    }
}
