﻿using UnityEngine;
using System.Collections;

public class UIView : MonoBehaviour {
	
	private UIController controlUI;

	public GameObject inventoryPanel;
	public GameObject pausePanel;
	public GameObject hud;
	public GameObject talkPanel;
	public GameObject statsPanel;
	public GameObject tradePanel;
	public GameObject enchantPanel;
	public GameObject lootPanel;
	public GameObject camera;

	byte gameState;
	
	private const byte PLAY = 0;
	private const byte INVENTORY = 1;
	private const byte PAUSE = 2;
	private const byte TALK = 3;
	private const byte TRADE = 4;
	private const byte STATS = 5;
	private const byte ENCHANT = 6;
	private const byte LOOT = 7;
	
	// Use this for initialization
	void Start () {
		gameState = PLAY;
	}
	
	// Update is called once per frame
	void Update () {

		controlUI = GetComponent<UIController>();
		gameState = controlUI.gameState;

		switch (gameState)
		{
			case PLAY:
				camera.GetComponent<MouseLook>().enabled = true;
				GetComponent<MouseLook>().enabled = true;
	
				camera.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>().enabled = false;

				Cursor.visible = false;

				hud.active = true;

				inventoryPanel.active = false;
				pausePanel.active = false;
				talkPanel.active = false;
				tradePanel.active = false;
				statsPanel.active = false;
				enchantPanel.active = false;
				lootPanel.active = false;
				break;

			case INVENTORY:
				camera.GetComponent<MouseLook>().enabled = false;
				GetComponent<MouseLook>().enabled = false;

				camera.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>().enabled = true;

				Cursor.visible = true;

				inventoryPanel.active = true;

				hud.active = false;
				break;

			case PAUSE:
				camera.GetComponent<MouseLook>().enabled = false;
				GetComponent<MouseLook>().enabled = false;

				camera.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>().enabled = true;

				Cursor.visible = true;

				pausePanel.active = true;

				hud.active = false;
				break;

			case TALK:
				camera.GetComponent<MouseLook>().enabled = false;
				GetComponent<MouseLook>().enabled = false;

				Cursor.visible = true;

				talkPanel.active = true;

				hud.active = false;
				break;

			case TRADE:
				camera.GetComponent<MouseLook>().enabled = false;
				GetComponent<MouseLook>().enabled = false;

				camera.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>().enabled = true;

				Cursor.visible = true;

				tradePanel.active = true;

				hud.active = false;
				break;

			case STATS:
				camera.GetComponent<MouseLook>().enabled = false;
				GetComponent<MouseLook>().enabled = false;

				camera.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>().enabled = true;

				Cursor.visible = true;

				statsPanel.active = true;

				hud.active = false;
				break;

			case ENCHANT:
				camera.GetComponent<MouseLook>().enabled = false;
				GetComponent<MouseLook>().enabled = false;
			
				camera.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>().enabled = true;
			
				Cursor.visible = true;
			
				enchantPanel.active = true;
			
				hud.active = false;
				break;

			case LOOT:
				camera.GetComponent<MouseLook>().enabled = false;
				GetComponent<MouseLook>().enabled = false;
			
				camera.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>().enabled = true;
			
				Cursor.visible = true;

				lootPanel.active = true;
			
				hud.active = false;
				break;
		}
	}
}
