﻿//Author Name: Yakov Mikhlin
//File Name: HealthPotion.cs
//Project Name: Polyrim
//Creation Date: 21/12/2015
//Modified Date: 21/12/2015
//Description: A health potion item piece, that has certain stats
using System;

namespace AssemblyCSharp
{
	public class HealthPotion : Item
	{
		private int healthAmount;
		private int uses;
        private int tierLevel;
        private string itemName;

        /// <summary>
        /// Initializes a new instance of the HealthPotion class.
        /// </summary>
        /// <param name="healthAmount">Sets the amount of health recovered by the potion</param>
        public HealthPotion (int healthAmount, int tierLevel)
		{
			this.healthAmount = healthAmount;
            this.tierLevel = tierLevel;
			uses = 1;
            AssignName();
		}

		//ACCESSORS

		/// <summary>
		/// Gets the health amount.
		/// </summary>
		/// <returns>The health amount.</returns>
		public override int GetHealthAmount()
		{
			return healthAmount;
		}

		/// <summary>
		/// Gets the number uses.
		/// </summary>
		/// <returns>The numberuses.</returns>
		public override int GetUses()
		{
			return uses;
		}

        /// <summary>
        /// Returns name of the item
        /// </summary>
        /// <returns>returns name of the item</returns>
        public override string GetName()
        {
            return itemName;
        }

        //MODIFIERS

        /// <summary>
        /// Sets the number uses.
        /// </summary>
        /// <param name="uses">Uses left</param>
        public override void SetUses(int uses)
		{
			this.uses = uses;
		}
		
		public override bool GetStackable()
		{
			return true;
		}

		public override int GetTierLevel()
		{
			return tierLevel;
		}

        //BEHAVIOURS

        /// <summary>
        /// Assigns the name of the item based on the class type and tier level
        /// </summary>
        protected override void AssignName()
        {
            itemName = "Health Potion";

            if (tierLevel == 1)
            {
                itemName = "Low " + itemName;
            }
            else if (tierLevel == 2)
            {
                itemName = "Moderate " + itemName;
            }
            else if (tierLevel == 3)
            {
                itemName = "High " + itemName;
            }
        }
    }
}

