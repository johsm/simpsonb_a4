﻿//Author Name: Yakov Mikhlin
//File Name: Item.cs
//Project Name: Polyrim
//Creation Date: 21/12/2015
//Modified Date: 21/12/2015
//Description: A general item class, meant to be used to keep track of the different items in the game.
using System;

namespace AssemblyCSharp
{
	public class Item
	{

		public Item ()
		{
		}

        //WEAPONS
        public virtual int GetDamage()
        {
            return 0;
        }
        
        public virtual int GetRangeValue()
        {
            return 0;
        }

        public virtual int GetBuffType()
        {
            return 0;
        }

        public virtual int GetBuffValue()
        {
            return 0;
        }

        public virtual void SetBuffType(int buffType)
        {

        }

        public virtual void SetBuffValue(int buffValue)
        {

        }

        //ARMOR
        public virtual int GetDefence()
        {
            return 0;
        }

        public virtual int GetHealthBuff()
        {
            return 0;
        }

        public virtual int GetManaBuff()
        {
            return 0;
        }

        //POTIONS
        public virtual int GetHealthAmount()
        {
            return 0;
        }

        public virtual int GetManaAmount()
        {
            return 0;
        }

        public virtual int GetUses()
        {
            return 0;
        }

		public virtual void SetUses(int uses)
		{
		}

        //Enchantment Crystals
        public virtual int GetNumOfCrystals()
        {
            return 0;
        }

        public virtual void SetNumOfCrystals(int numOfCrystals)
        {
            
        }

        //ALL
        public virtual string GetName()
        {
            return "";
        }

        protected virtual void AssignName()
        {

        }

        public virtual int GetTierLevel()
        {
            return 0;
        }

		public virtual bool GetStackable()
		{
			return false;
		}
    }
}

