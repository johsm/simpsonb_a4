//Author Name: Yakov Mikhlin
//File Name: EquipGenerator.cs
//Project Name: Polyrim
//Creation Date: 21/12/2015
//Modified Date: 22/12/2015
//Description: The item generator that creates a random item in the game, based on the stats of the player, and the enemy
using System;
namespace AssemblyCSharp
{
    public class EquipGenerator
    {
        //Holds the enemy level
        private int enemyLevel;

        //Holds the tier level
        public int tierLevel;

        //Holds the item type
        private int itemType;

        //Contains all the base stats of the items
        private int damageValues;
        private int rangeValues;
        private int defenceValues;
        private int healthBuffs;
        private int manaBuffs;
        private int healthPot;
        private int manaPot;

        //Contains the random generator
        private Random generator;

        //CONSTRUCTOR

        /// <summary>
        /// Initializes a new instance of the Equipment class class.
        /// </summary>
        /// <param name="classType">Sets the type of class that is being dropped</param>
        public EquipGenerator()
        {
            enemyLevel = 0;
            tierLevel = 0;
            generator = new Random();

            AssignBaseValues(tierLevel);
        }

        //MODIFIERS
        public void SetTierLevel(int tierLevel)
        {
            this.tierLevel = tierLevel;
        }

        public void SetEnemyLevel(int enemyLevel)
        {
            this.enemyLevel = enemyLevel;
        }

        //BEHAVIOURS

        /// <summary>
        /// Creates a random item after a drop occurs
        /// </summary>
        /// <returns>Returns the item created</returns>
        public Item GenerateItem()
        {
            itemType = SelectItem();

            if (itemType == 3)
            {
                return new Helmet(enemyLevel, defenceValues, ChooseClass(), tierLevel);
            }
            else if (itemType == 4)
            {
                return new Body(enemyLevel, defenceValues, healthBuffs, ChooseClass(), tierLevel);
            }
            else if (itemType == 5)
            {
                return new Feet(enemyLevel, defenceValues, manaBuffs, ChooseClass(), tierLevel);
            }
            else if (itemType == 0)
            {
                return new Sword(enemyLevel, damageValues, tierLevel);
            }
            else if (itemType == 1)
            {
                return new Staff(enemyLevel, damageValues, rangeValues, tierLevel);
            }
            else if (itemType == 2)
            {
                return new Gun(enemyLevel, damageValues, rangeValues, tierLevel);
            }
            else if (itemType == 6)
            {
                return new HealthPotion(healthPot, tierLevel);
            }
            else if (itemType == 7)
            {
                return new ManaPotion(manaPot, tierLevel);
            }
            else
            {
                return new EnchantmentCrystal();
            }
        }

        /// <summary>
        /// Randomly selects an item to create
        /// </summary>
        /// <returns>returns the number randomly created</returns>
        private int SelectItem()
        {
            int random;

            random = generator.Next(0, 9);

            return random;
        }

        /// <summary>
        /// Randomly selects a class for armor
        /// </summary>
        /// <returns>returns the number randomly created</returns>
        private int ChooseClass()
        {
            int random;

            random = generator.Next(0, 3);

            return random;
        }



        /// <summary>
        /// Assigns the base values of the stats on weapons, armor, and potions
        /// </summary>
        public void AssignBaseValues(int tL)
        {
            if (tL == 1)
            {
                damageValues = 5;
                defenceValues = 5;
                rangeValues = 2;
                healthBuffs = 20;
                manaBuffs = 20;
                healthPot = 50;
                manaPot = 25;
            }
            else if (tL == 2)
            {
                damageValues = 10;
                defenceValues = 10;
                rangeValues = 4;
                healthBuffs = 30;
                manaBuffs = 30;
                healthPot = 150;
                manaPot = 100;
            }
            else if (tL == 3)
            {
                damageValues = 15;
                defenceValues = 15;
                rangeValues = 6;
                healthBuffs = 40;
                manaBuffs = 40;
                healthPot = 300;
                manaPot = 200;
            }
        }
    }
}