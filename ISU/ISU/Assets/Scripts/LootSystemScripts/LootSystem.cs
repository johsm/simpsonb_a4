﻿//Author Name: Yakov Mikhlin
//File Name: LootSystem.cs
//Project Name: Polyrim
//Creation Date: 21/12/2015
//Modified Date: 22/12/2015
//Description: The loot system of the game, meant to randomly generate a new item based on probability when the enemy is killed
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AssemblyCSharp
{
    class LootSystem
    {
        private int tierLevel;
        private int enemyLevel;

        private EquipGenerator itemCreate;
        private Random generator;
        private int randomNum;

        private int coinAmount;

        private Item newItem;

        //CONSTRUCTOR
        /// <summary>
        /// The constructor of the lootsystem class
        /// </summary>
        /// <param name="tierLevel">The tier level in the game</param>
        /// <param name="classType">The class type of the player</param>
        /// <param name="enemyLevel">The level of the enemy killed</param>
        public LootSystem()
        {
            tierLevel = 1;
            enemyLevel = 1;
            itemCreate = new EquipGenerator();
            newItem = null;

            generator = new Random();
        }

        //MODIFIERS

        /// <summary>
        /// Sets the new enemyLevel
        /// </summary>
        /// <param name="enemyLevel">New enemyLevel</param>
        public void SetEnemyLevel(int enemyLevel)
        {
            this.enemyLevel = enemyLevel;
            itemCreate.SetEnemyLevel(enemyLevel);
        }

        /// <summary>
        /// Setst the new tierLevel
        /// </summary>
        /// <param name="tierLevel">New tier level</param>
        public void SetTierLevel(int tierLevel)
        {
            this.tierLevel = tierLevel;
            itemCreate.SetTierLevel(tierLevel);
        }            

        //BEHAVIOURS

        /// <summary>
        /// Checks to see if the a drop is occurring, and if so, returns what the drop is
        /// </summary>
        /// <returns>Returns the item that was dropped</returns>
        public Item CheckDrop()
        {
            randomNum = GenerateRandom();

            if (randomNum == 0)
            {
				RandomItem();
            }
            else
            {
                newItem = new Item();
            }

            return newItem;
        }

		public Item RandomItem()
		{
            SetTierLevel(tierLevel);
            itemCreate.AssignBaseValues(itemCreate.tierLevel);
			newItem = itemCreate.GenerateItem();
			return newItem;
		}
        

        /// <summary>
        /// Gets a certain number of coins that can be dropped by the monster
        /// </summary>
        /// <returns>Returns a certain number of coins</returns>
        public int CoinAmountReturn()
        {
            coinAmount = 0;

            if (tierLevel == 1)
            {
                coinAmount = generator.Next(20, 35);
            }
            else if (tierLevel == 2)
            {
                coinAmount = generator.Next(50, 80);
            }
            else if (tierLevel == 3)
            {
                coinAmount = generator.Next(100, 150);
            }

            return coinAmount;
        }


        /// <summary>
        /// A random generator to add probability to the game
        /// </summary>
        /// <returns>Returns a random number</returns>
        private int GenerateRandom()
        {
            return generator.Next(0, 10);
        }

    }
}
