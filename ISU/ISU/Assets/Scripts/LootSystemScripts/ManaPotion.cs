﻿//Author Name: Yakov Mikhlin
//File Name: ManaPotion.cs
//Project Name: Polyrim
//Creation Date: 21/12/2015
//Modified Date: 21/12/2015
//Description: A mana potion item piece, that has certain stats
using System;

namespace AssemblyCSharp
{
	public class ManaPotion : Item
	{
		private int manaAmount;
		private int uses;
        private int tierLevel;
        private string itemName;

		/// <summary>
		/// Initializes a new instance of the ManaPotion class.
		/// </summary>
		/// <param name="manaPotion">Sets the amount of mana recovered by the potion</param>
		public ManaPotion (int manaAmount, int tierLevel)
		{
			this.manaAmount = manaAmount;
            this.tierLevel = tierLevel;
			uses = 1;
            AssignName();
		}

		//ACCESSORS

		/// <summary>
		/// Gets the mana amount.
		/// </summary>
		/// <returns>The mana amount.</returns>
		public override int GetManaAmount()
		{
			return manaAmount;
		}

		/// <summary>
		/// Gets the number uses.
		/// </summary>
		/// <returns>The numberuses.</returns>
		public override int GetUses()
		{
			return uses;
		}

        /// <summary>
        /// Returns name of the item
        /// </summary>
        /// <returns>returns name of the item</returns>
        public override string GetName()
        {
            return itemName;
        }

        //MODIFIERS

        /// <summary>
        /// Sets the number uses.
        /// </summary>
        /// <param name="uses">Uses left</param>
        public override void SetUses(int uses)
		{
			this.uses = uses;
		}
		
		public override bool GetStackable()
		{
			return true;
		}
		
		public override int GetTierLevel()
		{
			return tierLevel;
		}

        //BEHAVIOURS

        /// <summary>
        /// Assigns the name of the item based on the class type and tier level
        /// </summary>
        protected override void AssignName()
        {
            itemName = "Mana Potion";

            if (tierLevel == 1)
            {
                itemName = "Low " + itemName;
            }
            else if (tierLevel == 2)
            {
                itemName = "Moderate " + itemName;
            }
            else if (tierLevel == 3)
            {
                itemName = "High " + itemName;
            }
        }
    }
}

