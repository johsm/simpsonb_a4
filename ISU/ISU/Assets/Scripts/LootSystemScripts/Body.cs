﻿//Author Name: Yakov Mikhlin
//File Name: Body.cs
//Project Name: Polyrim
//Creation Date: 21/12/2015
//Modified Date: 21/12/2015
//Description: A body armor item piece, that has certain stats
using System;

namespace AssemblyCSharp
{
	public class Body : Item
	{
		private int defence;
		private int baseDefence;
		private int healthBuff;
		private int enemyLevel;
		private int classType;
        private int tierLevel;
        private string itemName;

        /// <summary>
        /// Initializes a new instance of the Body class.
        /// </summary>
        /// <param name="enemyLevel">Level of the enemy</param>
        /// <param name="baseDefence">Base defence value</param>
        public Body (int enemyLevel, int baseDefence, int healthBuff, int classType, int tierLevel)
		{
			this.enemyLevel = enemyLevel;
			this.baseDefence = baseDefence;
			this.healthBuff = healthBuff;
			this.classType = classType;
            this.tierLevel = tierLevel;
            AssignDefence();
            AssignName();
		}

        //ACCESSORS

        /// <summary>
        /// Gets the defence value of the armor piece
        /// </summary>
        /// <returns>Returns the defence value</returns>
        public override int GetDefence()
        {
            return defence;
        }

        /// <summary>
        /// Returns the health buff of the item piece
        /// </summary>
        /// <returns>Returns the healthbuff value</returns>
        public override int GetHealthBuff()
        {
            return healthBuff;
        }

        /// <summary>
        /// Gets the tierlevel of the item
        /// </summary>
        /// <returns>returns the tierlevel of the item</returns>
        public override int GetTierLevel()
        {
            return tierLevel;
        }

        /// <summary>
        /// Returns name of the item
        /// </summary>
        /// <returns>returns name of the item</returns>
        public override string GetName()
        {
            return itemName;
        }
		
		/// <summary>
		/// Returns classtype of the item
		/// </summary>
		/// <returns>returns classtype of the item</returns>
		public int GetClassType()
		{
			return classType;
		}
		
        /// <summary>
        /// Assigns the defence value of the body
        /// </summary>
        private void AssignDefence()
		{
			defence = enemyLevel * baseDefence;
		}
        

        /// <summary>
        /// Assigns the name of the item based on the class type and tier level
        /// </summary>
        protected override void AssignName()
        {
            itemName = "Body";

            if (classType == 0)
            {
                itemName = "Warrior " + itemName;
            }
            else if (classType == 1)
            {
                itemName = "Wizard " + itemName;
            }
            else if (classType == 2)
            {
                itemName = "Marksman " + itemName;
            }

            if (tierLevel == 1)
            {
                itemName = "Low " + itemName;
            }
            else if (tierLevel == 2)
            {
                itemName = "Moderate " + itemName;
            }
            else if (tierLevel == 3)
            {
                itemName = "High " + itemName;
            }
        }
    }
}

