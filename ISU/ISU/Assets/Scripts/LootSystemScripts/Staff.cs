﻿//Author Name: Yakov Mikhlin
//File Name: Staff.cs
//Project Name: Polyrim
//Creation Date: 21/12/2015
//Modified Date: 21/12/2015
//Description: A staff weapon item piece, that has certain stats
using System;

namespace AssemblyCSharp
{
	public class Staff : Item
	{
		private int damage;
		private int baseDamage;
		private int range;
		private int enemyLevel;
        private int tierLevel;
        private int buffValue;
        private int buffType;
        private string itemName;

        //CONSTRUCTOR

        /// <summary>
        /// Initializes a new instance of the Staff class.
        /// </summary>
        /// <param name="enemyLevel">Level of the enemy</param>
        /// <param name="baseDamage">Base damage of the staff</param>
        /// <param name="range">The range of the weapon</param>
        public Staff (int enemyLevel, int baseDamage, int range, int tierLevel)
		{
			this.enemyLevel = enemyLevel;
			this.baseDamage = baseDamage;
            this.tierLevel = tierLevel;
			this.range = range;
            buffType = 0;
            buffValue = 0;
            AssignDamage ();
            AssignName();
		}

        //MODIFIERS

        /// <summary>
        /// Sets the buffvalue of the item
        /// </summary>
        /// <param name="buffValue">the buffvalue of the item</param>
        public override void SetBuffValue(int buffValue)
        {
            this.buffValue = buffValue;
        }

        /// <summary>
        /// Sets the type of buff on the item
        /// </summary>
        /// <param name="buffType">The buff type of the item</param>
        public override void SetBuffType(int buffType)
        {
            this.buffType = buffType;
        }

        //ACCESSORS

        /// <summary>
        /// Returns the range value of the weapon for stats
        /// </summary>
        /// <returns>Returns the range value</returns>
        public override int GetRangeValue()
        {
            return range;
        }

        /// <summary>
        /// Gets the buffvalue of the weapon
        /// </summary>
        /// <returns>Returns the buffvalue of the item</returns>
        public override int GetBuffValue()
        {
            return buffValue;
        }

        /// <summary>
        /// Gets the type of buff on the item
        /// </summary>
        /// <returns>returns the value of the buff type</returns>
        public override int GetBuffType()
        {
            return buffType;
        }

        /// <summary>
        /// Gets the damage of the item
        /// </summary>
        /// <returns>returns the damage of the item</returns>
        public override int GetDamage()
        {
            return damage;
        }

        /// <summary>
        /// Gets the tierlevel of the item
        /// </summary>
        /// <returns>returns the tierlevel of the item</returns>
        public override int GetTierLevel()
        {
            return tierLevel;
        }

        /// <summary>
        /// Returns name of the item
        /// </summary>
        /// <returns>returns name of the item</returns>
        public override string GetName()
        {
            return itemName;
        }

        //BEHAVIOURS

        /// <summary>
        /// Assigns the damage using the base damage of the weapon, and the enemylevel.
        /// </summary>
        private void AssignDamage()
		{
			damage = baseDamage * enemyLevel;
		}

        /// <summary>
        /// Assigns the name of the item based on the tier level
        /// </summary>
        protected override void AssignName()
        {
            itemName = "Staff";

            itemName = "Level " + enemyLevel + " " + itemName;

            if (tierLevel == 1)
            {
                itemName = "Low " + itemName;
            }
            else if (tierLevel == 2)
            {
                itemName = "Moderate " + itemName;
            }
            else if (tierLevel == 3)
            {
                itemName = "High " + itemName;
            }            
        }
    }
}

