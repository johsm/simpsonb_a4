﻿//Author Name: Yakov Mikhlin
//File Name: EnchantmentCrystal.cs
//Project Name: Polyrim
//Creation Date: 21/12/2015
//Modified Date: 21/12/2015
//Description: An enchantment crystal item piece
using System;

namespace AssemblyCSharp
{
    class EnchantmentCrystal : Item
    {
        private int numOfCrystals;
        private string itemName;

        public EnchantmentCrystal()
        {
            numOfCrystals = 1;
            itemName = "Enchantment Crystal";
        }

        //ACCESSOR

        /// <summary>
        /// Gets the number of number of crystals currently in the inventory
        /// </summary>
        /// <returns>Returns the number of crystals</returns>
        public override int GetNumOfCrystals()
        {
            return numOfCrystals;
        }

        //MODIFIER

        /// <summary>
        /// Sets the new number of crystals present
        /// </summary>
        /// <param name="numOfCrystals">The new number of crystals present</param>
        public override void SetNumOfCrystals(int numOfCrystals)
        {
            this.numOfCrystals = numOfCrystals;
        }

		/// <summary>
		/// Gets whether the item is stackable.
		/// </summary>
		/// <returns><c>true</c>, if the item is stackable, <c>false</c> otherwise.</returns>
		public override bool GetStackable()
		{
			return true;
		}

        //BEHAVIOURS

        /// <summary>
        /// Returns name of the item
        /// </summary>
        /// <returns>returns name of the item</returns>
        public override string GetName()
        {
            return itemName;
        }
    }
}
