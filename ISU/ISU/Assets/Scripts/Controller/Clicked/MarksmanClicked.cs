﻿using UnityEngine;
using System.Collections;

public class MarksmanClicked : MonoBehaviour {

    public GameObject player;
    private Character character;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        character = player.GetComponent<Character>();

    }

    //
    //
    //
    public void Clicked()
    {
        character.SetClass(new MarksmanClass());
    }
}
