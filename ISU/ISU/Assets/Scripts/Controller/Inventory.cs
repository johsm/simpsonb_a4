﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {

	//Holds the items in the inventory and their sprites
	private Item[,] items;
	private Sprite[,] itemSprites;
	public Item[] equipment;
	private Sprite[] equipmentSprites;

	//Holds the loot system
	private LootSystem lootSystem;

	//Holds the item that the player last clicked and its location
	private bool didClickItem = false;
	private int clickedItemX = NO_VALUE;
	private int clickedItemY = NO_VALUE;

	//Holds the panels for the inventory
	public GameObject inventoryPanel;
	public GameObject slotPanel;

    //Holds the panels for the enchanting inventory
    public GameObject enchantingPanel;
    public GameObject enchantingSlotPanel;

	//The coin text
	public Text coinText;

	//Holds the current stats of the player
	public Character character = new Character();
	
    //sprites for each of the item thumbnails
	public Sprite[] helmetSprites;
	public Sprite[] bodySprites;
	public Sprite[] feetSprites;
	public Sprite crystalSprite;
	public Sprite[] gunSprites;
	public Sprite[] healthSprites;
	public Sprite[] manaSprites;
	public Sprite[] staffSprites;
	public Sprite[] swordSprites;

    //constants to use when getting the length of the rows and columns
    private const int COLUMN = 0;
    private const int ROW = 1;

    //constants to represent the length of the rows and columns
    private const int COLUMN_MAX = 5;
    private const int ROW_MAX = 4;

    //a constant to show a right click input function
    private const int RIGHT_CLICK = 1;

    //constants to represent different class requirements
    private const int WARRIOR = 0;
    private const int WIZARD = 1;
    private const int MARKSMAN = 2;
    private const int ANY_CLASS = -1;

    //constants to represent different parts of equipment slots
    private const int HEAD = 0;
    private const int BODY = 1;
    private const int FEET = 2;
    private const int WEAPON = 3;

    //general constant of 0 for no value of a variable (back to 0)
    private const int NO_VALUE = 0;

    //Holds the item that is being enchanted
    public Item enchantItem = new Item();
    private Sprite enchantItemSprite = null;

    public int numOfCrystals = 0;
    public bool enchantingComplete = false;

    //Used for detecting when the inventory closes.
    public bool inventoryIsOpen = false;

	//Use this for initialization
	void Start () 
    {
		//Create the item and item sprites arrays
		items = new Item[COLUMN_MAX, ROW_MAX];
		itemSprites = new Sprite[COLUMN_MAX, ROW_MAX];

		//Create the equipment and equipment sprites arrays
		equipment = new Item[4];
		equipmentSprites = new Sprite[4];

		//Create the loot system
		lootSystem = new LootSystem();
	}

	/// <summary>
	/// Adds the item to the inventory.
	/// </summary>
	/// <returns>true, if item was added, false otherwise.</returns>
	/// <param name="item">the item to add</param>
	public bool AddItem(Item item)
	{
		//If it's an empty item, don't add it
		if(item.GetName() == "")
		{
			return false;
		}

		if (item == null) 
		{
			return false;
		}

		//Check to see if this type of item is already in the inventory
		bool foundItem = false;

		//Only check if the item can be stacked
		if(item.GetStackable())
		{
			for(int i = NO_VALUE; i < items.GetLength(COLUMN); i++)
			{
				for(int j = NO_VALUE; j < items.GetLength(ROW); j++)
				{
					//If the current item is not null, check to see if it is the same
					if(items[i, j] != null)
					{
						//Two items are the same if the types are the same and the tiers are the same
						if(items[i, j].GetType() == item.GetType() && 
						   items[i, j].GetTierLevel() == item.GetTierLevel())
						{
							//If this is a crystal, add one to number of crystals instead
							if(item is EnchantmentCrystal)
							{
								items[i, j].SetNumOfCrystals(items[i, j].GetNumOfCrystals() + 1);
							}
							else
							{
								//Add one to the number of uses on this item
								items[i, j].SetUses(items[i, j].GetUses() + 1);
							}

							//Set that we found the item so we don't add it again
							foundItem = true;
							break;
						}
					}
				}

				//If we already found an item, we can stop the loop
				if(foundItem)
				{
					break;
				}
			}
		}

		//If we didn't find an item yet, find an open spot and put it in there
		bool foundSpot = false;
        if(!foundItem)
		{
			//Find an empty spot in the inventory by checking each spot
			for(int j = NO_VALUE; j < items.GetLength(ROW); j++)
	        {
	            for(int i = NO_VALUE; i < items.GetLength(COLUMN); i++)
				{
					//This spot is empty
					if(items[i, j] == null)
					{
                        //determine that a spot was found
						foundSpot = true;

						//Find the sprite for the item and set the added item to the current open space
						Sprite itemSprite = SpriteForItem(item);
						itemSprites[i, j] = itemSprite;
						items[i, j] = item;
						break;
					}
				}

				//If we already put the item in a spot, we can stop the loop
				if(foundSpot)
				{
					break;
				}
			}
		}

		if(foundItem || foundSpot)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/// <summary>
	/// Handles when the user clicks on an item.
	/// </summary>
	/// <param name="num">The item number</param>
	public void ItemClicked(int num)
	{
		//Find the index of the parts of the arrays.
		int i = num % items.GetLength(COLUMN);
		int j = num / items.GetLength(COLUMN);

		//If the player clicked an item before, we should swap it with this item
		if(didClickItem)
		{
			//Only move them if the two positions are different
			if(clickedItemX != i || clickedItemY != j)
			{
				//Swap the items
				Item oldItem = items[clickedItemX, clickedItemY];
				items[clickedItemX, clickedItemY] = items[i, j];
				items[i, j] = oldItem;

				//Swap the sprites
				Sprite oldSprite = itemSprites[clickedItemX, clickedItemY];
				itemSprites[clickedItemX, clickedItemY] = itemSprites[i, j];
				itemSprites[i, j] = oldSprite;
			}

			//Reset the variables
			didClickItem = false;
			clickedItemX = NO_VALUE;
			clickedItemY = NO_VALUE;
		}
        
        //if the spot is null still switch it to that position
		else if(items[i, j] != null)
		{
			didClickItem = true;
			clickedItemX = i;
			clickedItemY = j;
		}
	}

	/// <summary>
	/// Handles when the user clicks on equipment.
	/// </summary>
	/// <param name="num">The equipment number.</param>
	public void EquipmentClicked(int num)
	{
		//If the equipment slot they clicked on had something in it
		if(equipment[num] != null)
		{
			//Add the equipment to the inventory
			Item item = equipment[num];
			bool itemAdded = AddItem(item);

			//If the item was added to the inventory (if there was room), remove it from equipment
			if(itemAdded)
			{
				//Set the equipment item and sprite to null
				equipment[num] = null;
                equipment[3] = null;
                equipmentSprites[num] = null;

				//reset the defence, health, mana, and damage buffs of each item
				if(item is Helmet)
				{
					character.helmetBuff = NO_VALUE;
				}
				else if(item is Body)
				{
					character.bodyBuff = NO_VALUE;
                    character.healthBuff = NO_VALUE;
				}
				else if(item is Feet)
				{
					character.feetBuff = NO_VALUE;
                    character.manaBuff = NO_VALUE;
                }
				else if(item is Gun || item is Sword || item is Staff)
				{
					character.damage = NO_VALUE;
				}
			}
		}
	}

    /// <summary>
    /// Removes the item the user clicked from the inventory
    /// </summary>
	public void DropClicked()
	{
        //if the user clicks this item set the item and the sprite to null
		if(didClickItem)
		{
			items[clickedItemX, clickedItemY] = null;
			itemSprites[clickedItemX, clickedItemY] = null;

			didClickItem = false;
		}
	}

	/// <summary>
	/// Sorts the items by removing gaps in the middle
	/// </summary>
	public void Sort()
	{
		//Stores whether there were empty spots before this item
		bool emptySpotsBefore = false;

		//Go by row first instead of column so things get sorted by row first
		for(int j = NO_VALUE; j < items.GetLength(ROW); j++)
        {
            for(int i = NO_VALUE; i < items.GetLength(COLUMN); i++)
			{
				//Only move items if there are empty spots before it
				if(items[i, j] == null)
				{
					emptySpotsBefore = true;
				}
				else if(emptySpotsBefore)
				{
					MoveItem(i, j);
				}
			}
		}
	}

	/// <summary>
	/// Moves the item to the first open spot.
	/// </summary>
	/// <param name="x">The first dimension of the array.</param>
	/// <param name="y">The second dimension of the array.</param>
	void MoveItem(int x, int y)
	{
		//Store whether the item was moved yet
		bool itemMoved = false;

		//Go row first so we sort by row before column
		for(int j = NO_VALUE; j < items.GetLength(ROW); j++)
        {
            for(int i = NO_VALUE; i < items.GetLength(COLUMN); i++)
			{
				//If the spot is open, move this item into it
				if(items[i, j] == null)
				{
					items[i, j] = items[x, y];
					itemSprites[i, j] = itemSprites[x, y];

					//Set the old spot to null
					items[x, y] = null;
					itemSprites[x, y] = null;
					itemMoved = true;
					break;
				}
			}

			//If the item was moved, we can stop the loop
			if(itemMoved)
			{
				break;
			}
		}
	}

	/// <summary>
	/// Returns the sprite for the item.
	/// </summary>
	/// <returns>The sprite for the item</returns>
	/// <param name="item">The current item</param>
	Sprite SpriteForItem(Item item)
	{
		//Stores the sprite for this item
		Sprite itemSprite = helmetSprites[0];

        //if the item is a helmet retrieve the correct sprite depending on the class
		if(item is Helmet)
		{
			Helmet helmet = (Helmet)item;
			itemSprite = helmetSprites[helmet.GetClassType()];
		}
		else if(item is Body) //if the item is a body armour retrieve the correct sprite depending on the class
		{
			Body body = (Body)item;
			itemSprite = bodySprites[body.GetClassType()];
		}
		else if(item is Feet) //if the item is a foot armour retrieve the correct sprite depending on the class
		{
			Feet feet = (Feet)item;
			itemSprite = feetSprites[feet.GetClassType()];
		}
		else if(item is EnchantmentCrystal) //if the item is an enchanting crystal retrieve the correct sprite
		{
			itemSprite = crystalSprite;
		}
		else if(item is Gun) //if the item is a gun retrieve the correct sprite depending on the tier level
		{
			itemSprite = gunSprites[item.GetTierLevel() - 1];
		}
		else if(item is HealthPotion) //if the item is a hp pot retrieve the correct sprite depending on the tier level
		{
			itemSprite = healthSprites[item.GetTierLevel() - 1];
		}
		else if(item is ManaPotion) //if the item is a mana pot retrieve the correct sprite depending on the tier level
		{
			itemSprite = manaSprites[item.GetTierLevel() - 1];
		}
		else if(item is Staff) //if the item is a staff retrieve the correct sprite depending on the tier level
		{
			itemSprite = staffSprites[item.GetTierLevel() - 1];
		}
		else if(item is Sword) //if the item is a sword retrieve the correct sprite depending on the tier level
		{
			itemSprite = swordSprites[item.GetTierLevel() - 1];
		}

		return itemSprite;
	}

    /// <summary>
    /// Used to draw sprites and GUI elements
    /// </summary>
	private void OnGUI()
	{
        //If the inventory is not open, do not draw the GUI
        if (!inventoryPanel.activeSelf && !enchantingPanel.activeSelf)
        {
            //If the inventory was open, we need to handle closing it.
            if (inventoryIsOpen)
            {
                inventoryIsOpen = false;
                //If there is an item in the enchant slot, add it to an open space.
                if (enchantItem != null && AddItem(enchantItem))
                {
                    enchantItem = null;
                    enchantItemSprite = null;
                }
            }
            return;
        }

        inventoryIsOpen = true;

        bool inEnchanting = false;
        if (enchantingPanel.activeSelf)
        {
            inEnchanting = true;
        }

		//The start of the inventory
		Vector3 startPos = slotPanel.transform.position;

		//Draw equipment based on each slot
		for(int i = NO_VALUE; i < equipmentSprites.Length; i++)
		{
			//If this is null, don't draw it
			if(equipment[i] == null || equipmentSprites[i] == null)
			{
				continue;
			}

            //set floats for the location of the equipment sprite
            float xPos = startPos.x - 405f;
            float yPos = startPos.y - 255f + i * 102f;

            if (inEnchanting)
            {
                xPos = startPos.x - 510f;
                yPos = startPos.y - 225f + i * 102f;
            }

			Sprite sprite = equipmentSprites[i];

			//Draw the texture in the middle of the box
			GUI.DrawTexture(new Rect(xPos + 10, yPos + 10, 80, 80), sprite.texture);
		}

		//Draw inventory items
		for(int i = NO_VALUE; i < itemSprites.GetLength(COLUMN); i++)
		{
			for(int j = NO_VALUE; j < itemSprites.GetLength(ROW); j++)
			{
				//If the item is null, don't draw it
				if(items[i, j] == null || itemSprites[i, j] == null) 
				{
					continue;
				}

                //sets floats for the location of the inventory item
                float xPos = startPos.x - 245f + i * 102f;
                float yPos = startPos.y - 245f + j * 102f;

                if (inEnchanting)
                {
                    xPos = startPos.x - 350f + i * 102f;
                    yPos = startPos.y - 220f + j * 102f;
                }

				Sprite sprite = itemSprites[i, j];

				//Draw the texture in the middle of the box
				GUI.DrawTexture(new Rect(xPos, yPos, 80, 80), sprite.texture);

				//If there is more than one use, draw a label for it
				if(items[i, j].GetUses() > 1)
				{
					GUI.Label(new Rect(xPos + 70, yPos - 5, 20, 20), items[i, j].GetUses().ToString());
				}

				//If this is a crystal and there is more than one, do the same
				if(items[i, j].GetNumOfCrystals() > 1)
				{
					GUI.Label(new Rect(xPos + 70, yPos - 5, 20, 20), items[i, j].GetNumOfCrystals().ToString());
				}
			}
		}

		if(enchantItem != null)
		{
			GUI.DrawTexture(new Rect(startPos.x + 315, startPos.y - 215, 80, 80), enchantItemSprite.texture);
		}

	}

<<<<<<< HEAD
	void Update() {
=======
    public void EnchantItemClicked()
    {
        if (AddItem(enchantItem))
        {
            enchantItem = new Item();
            enchantItemSprite = null;
        }
    }
    
    /// <summary>
    /// Constantly updates the inventory elements
    /// </summary>
	private void Update()
	{

        bool inEnchanting = false;
        if (enchantingPanel.activeSelf)
        {
            inEnchanting = true;
        }
>>>>>>> 0c42ab679fd4c3c93194cd7e1e1284341e7c9f03

		//Hotkeys for testing
		if(Input.GetKeyDown(KeyCode.Alpha1))
		{
			AddItem(new EnchantmentCrystal());            
		}
		else if(Input.GetKeyDown(KeyCode.Alpha2))
		{
			AddItem(new HealthPotion(10, 1));
		}
		else if(Input.GetKeyDown(KeyCode.Alpha3))
		{
			AddItem(new ManaPotion(10, 1));
		}
		else if(Input.GetKeyDown(KeyCode.Alpha4))
		{
			lootSystem.SetTierLevel(1);
			character.coins += lootSystem.CoinAmountReturn();
			AddItem(lootSystem.RandomItem());
		}
		else if(Input.GetKeyDown(KeyCode.Alpha5))
		{
			lootSystem.SetTierLevel(2);
			character.coins += lootSystem.CoinAmountReturn();
			AddItem(lootSystem.RandomItem());
		}
		else if(Input.GetKeyDown(KeyCode.Alpha6))
		{
			lootSystem.SetTierLevel(3);
			character.coins += lootSystem.CoinAmountReturn();
			AddItem(lootSystem.RandomItem());
		}
        else if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            lootSystem.SetTierLevel(1);
            character.coins += lootSystem.CoinAmountReturn();
            AddItem(lootSystem.CheckDrop());
        }

		//There is no way to handle direct right click on a button, so instead it is determined if the user has left clicked it first
		if(didClickItem && Input.GetMouseButtonDown(RIGHT_CLICK))
		{
			bool isEquipment = false;
			int equipmentPos = HEAD;
			int classRequirement = ANY_CLASS;
			Item clickedItem = items[clickedItemX, clickedItemY];

            //Set the buffs
            if (inEnchanting)
            {
                if (clickedItem is Gun || clickedItem is Sword || clickedItem is Staff)
                {
                    Item oldEnchantItem = enchantItem;
                    Sprite oldEnchantSprite = enchantItemSprite;
                    //Put the clicked item in the weapon box.
                    enchantItem = clickedItem;
                    enchantItemSprite = itemSprites[clickedItemX, clickedItemY];
                    //Put the old item in the new item's old spot.
                    items[clickedItemX, clickedItemY] = oldEnchantItem;
                    itemSprites[clickedItemX, clickedItemY] = oldEnchantSprite;
                }
            }
			//if the equipped item is a helmet set its equipment slot to be filled and retrieve the defence buff
			else if(clickedItem is Helmet)
			{
				equipmentPos = HEAD;
				classRequirement = ((Helmet)clickedItem).GetClassType();
				character.helmetBuff = clickedItem.GetDefence();
				isEquipment = true;
			}
			else if(clickedItem is Body) //if the equipped item is a body piece set its equipment slot to be filled and retrieve the defence and hp buff
			{
				equipmentPos = BODY;
				classRequirement = ((Body)clickedItem).GetClassType();
				character.bodyBuff = clickedItem.GetDefence();
                character.healthBuff = clickedItem.GetHealthBuff();
				isEquipment = true;
			}
			else if(clickedItem is Feet) //if the equipped item is a foot piece set its equipment slot to be filled and retrieve the defence and mana buff
			{
				equipmentPos = FEET;
				classRequirement = ((Feet)clickedItem).GetClassType();
				character.feetBuff = clickedItem.GetDefence();
                character.manaBuff = clickedItem.GetManaBuff();
				isEquipment = true;
			}
			else if(clickedItem is Gun || clickedItem is Sword || clickedItem is Staff) //if the equipped item is a weapon set its equipment slot to be filled and retrieve the damage buff
			{
				equipmentPos = WEAPON;
				character.damage = clickedItem.GetDamage();
				isEquipment = true;

<<<<<<< HEAD
=======
                //if the item is a sword set the class requirement to be for a warrior
>>>>>>> 0c42ab679fd4c3c93194cd7e1e1284341e7c9f03
				if(clickedItem is Sword)
				{
					classRequirement = WARRIOR;
				}
				else if(clickedItem is Staff) //if the item is a staff set the class requirement to be for a wizard
				{
					classRequirement = WIZARD;
				}
				else if(clickedItem is Gun) //if the item is a gun set the class requirement to be for a marksman
				{
					classRequirement = MARKSMAN;
				}

			}
			else if(clickedItem is HealthPotion) //if the item is a health potion set the character hp to increase
			{
				HealthPotion healthPotion = (HealthPotion)clickedItem;
				character.health += healthPotion.GetHealthAmount();

				//Reduce the number of uses if there is more than one
				if(healthPotion.GetUses() > 1)
				{
					healthPotion.SetUses(healthPotion.GetUses() - 1);
				}			
				else //If there are no uses left, remove the item
				{
					items[clickedItemX, clickedItemY] = null;
					itemSprites[clickedItemX, clickedItemY] = null;
				}
            }
			else if(clickedItem is ManaPotion) //if the item is a mana potion set the character mana to increase
			{
				ManaPotion manaPotion = (ManaPotion)clickedItem;
				character.mana += manaPotion.GetManaAmount();

				//Reduce the number of uses if there is more than one
				if(manaPotion.GetUses() > 1)
				{
					manaPotion.SetUses(manaPotion.GetUses() - 1);
				}		
				else //If there are no uses left, remove the item
				{
					items[clickedItemX, clickedItemY] = null;
                    itemSprites[clickedItemX, clickedItemY] = null;
				}
			}

			//If this is equipment, we need to equip it
            if(isEquipment)
            {
				//Make sure that the player is the right class
				if(classRequirement == ANY_CLASS || 
				   (classRequirement == WARRIOR && character.playerClass is WarriorClass) ||
				   (classRequirement == WIZARD && character.playerClass is WizardClass) ||
				   (classRequirement == MARKSMAN && character.playerClass is MarksmanClass))
				{
                    //Swap the old equipped item and its sprites with this one
                    Item oldItem = equipment[equipmentPos];
                    Sprite oldSprite = equipmentSprites[equipmentPos];
                    equipment[equipmentPos] = clickedItem;
                    equipmentSprites[equipmentPos] = itemSprites[clickedItemX, clickedItemY];
                    items[clickedItemX, clickedItemY] = oldItem;
                    itemSprites[clickedItemX, clickedItemY] = oldSprite;

				}


			}

            //set the click item boolean to false, to show the equipping process is finished
			didClickItem = false;
		}

		//Update the coin text
		coinText.text = "G: " + character.coins;
	}

    /// <summary>
    /// Retrieves the number of crystals currently in the inventory
    /// </summary>
    public void GetNumOfCrystals()
    {
        //Looks for the crystal item in the inventory, and assigns the variable to the number of crystals
        for (int i = NO_VALUE; i < itemSprites.GetLength(COLUMN); i++)
        {
            for (int j = NO_VALUE; j < itemSprites.GetLength(ROW); j++)
            {
                if (items[i, j] is EnchantmentCrystal)
                {
                    numOfCrystals = items[i, j].GetNumOfCrystals();
                }
            }
        }
        //If it cannot find it, the default value is 0
    }


    /// <summary>
    /// Checks to see if the enchanting has completed, then adjusts the number of crystal items in the inventory
    /// </summary>
    public void CheckEnchanting()
    {
        //Checks if the enchanting has finished
        if (enchantingComplete == true)
        {
            //Goes through the loop to check for the crystal item
            for (int i = NO_VALUE; i < itemSprites.GetLength(COLUMN); i++)
            {
                for (int j = NO_VALUE; j < itemSprites.GetLength(ROW); j++)
                {
                    //If it is a crystal, it sets the new number of crystals of the crystals item
                    if (items[i, j] is EnchantmentCrystal)
                    {
                        items[i, j].SetNumOfCrystals(numOfCrystals);

                        //Checks to see if the number of crystals left is 0, if so, then delete the item and the sprite
                        if (numOfCrystals == 0)
                        {
                            items[i, j] = null;
                            itemSprites[i, j] = null;
                        }
                    }
                }
            }

            //Once it finishs, sets the enchanting succession to false
            enchantingComplete = false;
        }
    }
}
