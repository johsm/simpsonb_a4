﻿using UnityEngine;
using System.Collections;

public class BuyButton : MonoBehaviour {

	public GameObject tradePanel;
	private TradeView tradeView;

	// Update is called once per frame
	void Update () {
		
		tradeView = tradePanel.GetComponent<TradeView>();
	}

	public void Clicked()
	{
		tradeView.tradeState = 0;
	}
}
