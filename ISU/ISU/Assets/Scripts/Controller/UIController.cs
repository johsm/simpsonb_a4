﻿using UnityEngine;
using System.Collections;

public class UIController : MonoBehaviour {

	public byte gameState;

	public byte PLAY = 0;
    public byte INVENTORY = 1;
    public byte PAUSE = 2;
    public byte TALK = 3;
    public byte TRADE = 4;
    public byte STATS = 5;
    public byte ENCHANT = 6;
    public byte LOOT = 7;

	bool canPress;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.Escape) == true && gameState != PLAY && canPress == true)
		{
			canPress = false;
			gameState = PLAY;
		}

		if (Input.GetKeyDown(KeyCode.I) == true && gameState == PLAY && canPress == true)
		{
			canPress = false;
			gameState = INVENTORY;
		}
		else if (Input.GetKeyDown(KeyCode.I) == true && gameState == INVENTORY && canPress == true)
		{
			canPress = false;
			gameState = PLAY;
		}

		if (Input.GetKeyDown(KeyCode.Escape) == true && gameState == PLAY && canPress == true)
		{
			canPress = false;
			gameState = PAUSE;
		}

		if (Input.GetKeyDown(KeyCode.T) == true && gameState == PLAY && canPress == true)
		{
			canPress = false;
			gameState = TALK;
		}
		else if (Input.GetKeyDown(KeyCode.T) == true && gameState == TALK && canPress == true)
		{
			canPress = false;
			gameState = PLAY;
		}

		if (Input.GetKeyDown(KeyCode.E) == true && gameState == PLAY && canPress == true)
		{
			canPress = false;
			gameState = TRADE;
		}
		else if (Input.GetKeyDown(KeyCode.E) == true && gameState == TRADE && canPress == true)
		{
			canPress = false;
			gameState = PLAY;
		}

		if (Input.GetKeyDown(KeyCode.Tab) == true && gameState == PLAY && canPress == true)
		{
			canPress = false;
			gameState = STATS;
		}
		else if (Input.GetKeyDown(KeyCode.Tab) == true && gameState == STATS && canPress == true)
		{
			canPress = false;
			gameState = PLAY;
		}

		if (Input.GetKeyDown(KeyCode.F) == true && gameState == PLAY && canPress == true)
		{
			canPress = false;
			gameState = ENCHANT;
		}
		else if (Input.GetKeyDown(KeyCode.F) == true && gameState == ENCHANT && canPress == true)
		{
			canPress = false;
			gameState = PLAY;
		}

		if (Input.GetKeyDown(KeyCode.L) == true && gameState == PLAY && canPress == true)
		{
			canPress = false;
			gameState = LOOT;
		}
		else if (Input.GetKeyDown(KeyCode.L) == true && gameState == LOOT && canPress == true)
		{
			canPress = false;
			gameState = PLAY;
		}

		canPress = true;
	}
}
