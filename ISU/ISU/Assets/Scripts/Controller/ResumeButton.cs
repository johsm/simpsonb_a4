﻿using UnityEngine;
using System.Collections;

public class ResumeButton : MonoBehaviour {

	public GameObject player;
	private UIController controlUI;

	// Update is called once per frame
	void Update () {

		controlUI = player.GetComponent<UIController>();
	}

	public void Clicked()
	{
		controlUI.gameState = 0;
	}
}
