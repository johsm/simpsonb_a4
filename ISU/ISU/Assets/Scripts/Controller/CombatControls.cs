﻿using UnityEngine;
using System.Collections;

public class CombatControls : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {

	}

    public bool LeftMouseClick()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            return true;
        }
        return false;
    }

    public bool RightMouseClick()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            return true;
        }
        return false;
    }

    public bool MiddleMouseClick()
    {
        if (Input.GetButtonDown(""))
        {
            return true;
        }
        return false;
    }
}
