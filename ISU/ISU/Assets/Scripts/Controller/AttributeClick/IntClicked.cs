﻿using UnityEngine;
using System.Collections;

public class IntClicked : MonoBehaviour {

    public GameObject player;
    private Character character;

    public GameObject intButton;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        character = player.GetComponent<Character>();

        if (character.attributePointsToSpend > 0)
        {
            intButton.active = true;
        }
        else
        {
            intButton.active = false;
        }
    }

    //Pre:
    //Post:
    //Description: 
    public void Clicked()
    {
        character.intelligence++;
        character.attributePointsToSpend--;
    }

}
