﻿using UnityEngine;
using System.Collections;

public class DexClicked : MonoBehaviour {

    public GameObject player;
    private Character character;

    public GameObject dexButton;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        character = player.GetComponent<Character>();

        if (character.attributePointsToSpend > 0)
        {
            dexButton.SetActive(true);
        }
        else
        {
            dexButton.SetActive(false);
        }
    }

    //Pre:
    //Post:
    //Description: 
    public void Clicked()
    {
        character.dexterity++;
        character.attributePointsToSpend--;
    }
}
