﻿using UnityEngine;
using System.Collections;

public class StrClicked : MonoBehaviour {

    public GameObject player;
    private Character character;

    public GameObject strButton;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        character = player.GetComponent<Character>();

        if (character.attributePointsToSpend > 0)
        {
            strButton.active = true;
        }
        else
        {
            strButton.active = false;
        }
    }

    //Pre:
    //Post:
    //Description: 
    public void Clicked()
    {
        character.strength++;
        character.attributePointsToSpend--;
    }
}
