using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using AssemblyCSharp;

public class PlayerStats : MonoBehaviour {
	public float health = 100;
	public float healthBuff = 0;
	public float mana = 100;
	public float manaBuff = 0;

	public int coins = 0;

	public PlayerClass playerClass;

	public float helmetBuff = 0;
	public float bodyBuff = 0;
	public float feetBuff = 0;

	public float damage = 0;

	public Image healthBar;
	public Image manaBar;

	public PlayerStats()
	{
		playerClass = new WizardClass();
	}

	public float GetDefence()
	{
		return helmetBuff + bodyBuff + feetBuff;
	}

	void Update() {
		healthBar.fillAmount = (health + healthBuff) / 100;
		manaBar.fillAmount = (mana + manaBuff) / 100;
	}
}