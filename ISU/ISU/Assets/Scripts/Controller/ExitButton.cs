﻿using UnityEngine;
using System.Collections;

public class ExitButton : MonoBehaviour {

	public void Clicked()
	{
		Application.Quit();
	}
}
