﻿using UnityEngine;
using System.Collections;

public class SellButton : MonoBehaviour {

	public GameObject tradePanel;
	private TradeView tradeView;
	
	// Update is called once per frame
	void Update () {
		
		tradeView = tradePanel.GetComponent<TradeView>();
	}
	
	public void Clicked()
	{
		tradeView.tradeState = 1;
	}
}
