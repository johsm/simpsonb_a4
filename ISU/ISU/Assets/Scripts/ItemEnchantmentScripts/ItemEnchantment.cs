﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AssemblyCSharp
{
    class ItemEnchantment
    {
        //Variable for the choice
        private int choice;
        
        //Variable to hold the change value of the item
        private BuffChanges change;
        
        //Holds the number of crystals
        private int numOfCrystals;
        
        //Holds the tierlevel
        private int tierLevel;

        //Checks to see if the enchantment has succeeded
        private bool enchantSuccuss;

        //CONSTRUCTOR

        /// <summary>
        /// The constructor of the class, contains all the base values
        /// </summary>
        public ItemEnchantment()
        {
            choice = 0;
            numOfCrystals = 0;
            tierLevel = 0;
            enchantSuccuss = false;
            change = new BuffChanges();
        }

        /// <summary>
        /// Updates the tierlevel of the item
        /// </summary>
        /// <param name="tierLevel">new tier level is entered</param>
        public void SetTierLevel(int tierLevel)
        {
            this.tierLevel = tierLevel;
            change.SetTierLevel(tierLevel);
        }

        /// <summary>
        /// Sets the choice of the enchantment
        /// </summary>
        /// <param name="choice">the enchantment choice is passed</param>
        public void SetChoice(int choice)
        {
            this.choice = choice;
        }

        /// <summary>
        /// Updates number of crystals the player has
        /// </summary>
        /// <param name="numOfCrystals"></param>
        public void SetNumOfCrystals(int numOfCrystals)
        {
            this.numOfCrystals = numOfCrystals;
        }

        public void SetEnchantSuccess(bool eSuccess)
        {
            enchantSuccuss = eSuccess;
        }

        /// <summary>
        /// Gets the number of crystals the player has
        /// </summary>
        /// <returns>Returns the number of crystals the player has</returns>
        public int GetNumOfCrystals()
        {
            return numOfCrystals;
        }

        /// <summary>
        /// Gets the choice of the enchantment
        /// </summary>
        /// <returns>Returns choice value</returns>
        public int GetChoice()
        {
            return choice;
        }

        /// <summary>
        /// Gets if the enchantment has succeeded or not
        /// </summary>
        /// <returns>returns a bool if the enchantment is finished</returns>
        public bool GetEnchantSuccess()
        {
            return enchantSuccuss;
        }

        /// <summary>
        /// Calls which buff to use, depending on the tier of the weapon, the number of crystals the player needs, and the type of enchantment they wish to make
        /// </summary>
        /// <returns>Returns the enchantment buff value and changes the number of crystals the player has</returns>
        public int BuffValue()
        {
            //Goes through each tier level, where each level requires 10 more crystals than the previous amount
            if (tierLevel == 1)
            {
                if (numOfCrystals >= 5)
                {
                    //Subtracts the crystals used during the enchantment from the number of crystals the player currently has
                    numOfCrystals = numOfCrystals - 5;
                    enchantSuccuss = true;

                    //Based on the choice, returns buff value
                    if (choice == 1)
                    {
                        return change.FireBuff();
                    }
                    else if (choice == 2)
                    {
                        return change.IceBuff();
                    }
                    else if (choice == 3)
                    {
                        return change.ElectricityBuff();
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
            else if (tierLevel == 2)
            {
                if (numOfCrystals >= 10)
                {
                    numOfCrystals = numOfCrystals - 10;
                    enchantSuccuss = true;

                    if (choice == 1)
                    {
                        return change.FireBuff();
                    }
                    else if (choice == 2)
                    {
                        return change.IceBuff();
                    }
                    else if (choice == 3)
                    {
                        return change.ElectricityBuff();
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                if (numOfCrystals >= 15)
                {
                    numOfCrystals = numOfCrystals - 15;
                    enchantSuccuss = true;

                    if (choice == 1)
                    {
                        return change.FireBuff();
                    }
                    else if (choice == 2)
                    {
                        return change.IceBuff();
                    }
                    else if (choice == 3)
                    {
                        return change.ElectricityBuff();
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }

        }

        /// <summary>
        /// Used to state the number of crystals required for the enchantment to succeed
        /// </summary>
        /// <param name="tL">Insert the tierlevel of the weapon to figure out the number of crystals required</param>
        /// <returns>Returns the number of crystals required</returns>
        public int GetCrystalsRequired(int tL)
        {
            //Checks the tier level and returns the number
            if (tL == 1)
            {
                return 5;
            }
            else if (tL == 2)
            {
                return 10;
            }
            else if (tL == 3)
            {
                return 15;
            }
            //Returns 0 if there is no tierlevel
            else
            {
                return 0;
            }
        }
    }
}
