﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AssemblyCSharp
{
    class BuffChanges
    {
        private int fireBaseBuff;
        private int iceBaseBuff;
        private int electricityBaseBuff;
        private int tierLevel;
        
        public BuffChanges()
        {
            tierLevel = 0;
            fireBaseBuff = 5;
            iceBaseBuff = 5;
            electricityBaseBuff = 10;
        }
        

        public void SetTierLevel(int tierLevel)
        {
            this.tierLevel = tierLevel;
        }

        public int FireBuff()
        {
            return fireBaseBuff * tierLevel;
        }

        public int IceBuff()
        {
            return iceBaseBuff * tierLevel;
        }

        public int ElectricityBuff()
        {
            return electricityBaseBuff * tierLevel;
        }
    }
}
