﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;
using UnityEngine.UI;

public class Enchantment : MonoBehaviour
{
    //Variable that handles the enchanting of the item
    ItemEnchantment enchanter = new ItemEnchantment();

    //Holds the choice the player picked
    public int enchantChoice = 0;

    //Holds the instance of the inventory 
    public Inventory inventory;

    //Holds the descriptions of what the enchantments do
    public Text enchantDesc;

    // Use this for initialization
    void Start()
    {
        
    }



    // Update is called once per frame
    void Update()
    {
      
    }

    /// <summary>
    /// Updates the description of the text for the fire buff
    /// </summary>
    public void FireClicked()
    {
        enchantDesc.text = "Description: Enables the weapon to do damage overtime by a certain amount of damage based on the tier level.\n\n\nCrystals: " + enchanter.GetCrystalsRequired(inventory.enchantItem.GetTierLevel()); 
    }

    /// <summary>
    /// Updates description of the text for ice buff
    /// </summary>
    public void IceClicked()
    {
        enchantDesc.text = "Description: Enables the weapon to slow the enemy by a certain amount of damage based on the tier level.\n\n\nCrystals: " + enchanter.GetCrystalsRequired(inventory.enchantItem.GetTierLevel());
    }

    /// <summary>
    /// Updates description fo the text for electricity buff
    /// </summary>
    public void ElecClicked()
    {
        enchantDesc.text = "Description: Enables the weapon to do extra damage per hit depending on the tier level.\n\n\n\nCrystals: " + enchanter.GetCrystalsRequired(inventory.enchantItem.GetTierLevel());
    }

    /// <summary>
    /// Sets the enchantment choice the player picks
    /// </summary>
    /// <param name="enchantChoice">the enchantment choice is passed</param>
    public void SetEnchantChoice(int enchantChoice)
    {
        this.enchantChoice = enchantChoice;
    }
    
    /// <summary>
    /// Enchants the item that the user has selected to enchant
    /// </summary>
    public void EnchantItem()
    {
        //Checks first if the item is valid to enchant
        if (CheckItem(inventory.enchantItem))
        {
            //Checks if the item already has a buff on it or not
            if (inventory.enchantItem.GetBuffType() == 0)
            {
                //Sets the tier level of the weapon
                enchanter.SetTierLevel(inventory.enchantItem.GetTierLevel());

                //Sets the enchantment type
                enchanter.SetChoice(enchantChoice);

                //Updates number of crystals in the inventory
                inventory.GetNumOfCrystals();

                //Assigns it to the enchanter
                enchanter.SetNumOfCrystals(inventory.numOfCrystals);

                //Sets the buff value of the weapon which also reduces number of crystals, and sets that the enchantment has succeeded
                inventory.enchantItem.SetBuffValue(enchanter.BuffValue());

                //If the enchantment has succeeded then it continues
                if (enchanter.GetEnchantSuccess())
                {
                    //Sets the items type of buff to the enchantment choice
                    inventory.enchantItem.SetBuffType(enchanter.GetChoice());

                    //Sets the enchantment success to false to reset enchant
                    enchanter.SetEnchantSuccess(false);

                    //Sets the inventories new number of crystals as the number of crystals left, and sets the completion of enchantment to true
                    inventory.numOfCrystals = enchanter.GetNumOfCrystals();
                    inventory.enchantingComplete = true;

                    //Finally, updates num of crystals item value
                    inventory.CheckEnchanting();
                }
                else
                {
                    //If the enchantment didnt succeed, no enchantment will go onto the item
                    inventory.enchantItem.SetBuffType(0);
                }
            }

        }
    }

    /// <summary>
    /// Used to check the item being passed into the enchantment slot
    /// </summary>
    /// <param name="itemToEnchant">The item to enchant is passed</param>
    /// <returns>returns a true or false whether the item is okay to enchant or not</returns>
    bool CheckItem(Item itemToEnchant)
    {
        if (itemToEnchant is Sword || itemToEnchant is Gun || itemToEnchant is Staff)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}

