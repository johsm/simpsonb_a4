﻿using UnityEngine;
using System.Collections;

public class MarksmanClass : PlayerClass {

    private RaycastHit closeProxyDamage;
    private Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
    private Vector3 attackDirection;

    GameObject prefabProjectile;

    public MarksmanClass()
    {

    }

    // Use this for initialization
    void Start () {
        prefabProjectile = Resources.Load("projectile") as GameObject;
        //combatControl = new CombatControls();

        basicCoolDown = 1f;
        midCoolDown = 3f;
        ultimateCoolDown = 10f;

        basicManaCost = 0f;
        midManaCost = 150f;
        ultimateManaCost = 300f;

        cooldownTimer = 0f;

        distance = 0f;
        damage = 0f;
    }
	
	// Update is called once per frame
	void Update () {

        cooldownTimer += Time.deltaTime;

        if (cooldownTimer >= basicCoolDown)
        {
            firstPersonAnimation.SetBool("swing1", false);
            BaseAttack();
        }
        if (cooldownTimer >= midCoolDown)
        {
            MidPowerAttack();
        }
        if (cooldownTimer >= ultimateCoolDown)
        {
            firstPersonAnimation.SetBool("swing2", false);
            UltimateAttack();
        }
    }

    //Pre: 
    //Post: 
    //Description: 
    public override void BaseAttack()
    {
        if (Input.GetButtonDown("Fire1") && player.playerClass is MarksmanClass && isHudOpen() && player.equippedWeapon != null)
        {
            firstPersonAnimation.SetBool("swing1", true);
            cooldownTimer = 0f;

            damage = player.strength + player.equippedWeapon.GetDamage();
            player.mana -= basicManaCost;
            distance = Vector3.Distance(target.transform.position, transform.position);

            attackDirection = (target.transform.position - transform.position).normalized;

            //
            float direction = Vector3.Dot(attackDirection, transform.forward);

            Debug.Log(direction);

            if (distance < 2.5 && direction > 0)
            {
                Debug.Log("Hit: " + damage + "\nDistance: " + distance);
            }
            else
            {
                Debug.Log("No hit\nDistance: " + distance);
            }

        }
    }

    //Pre: 
    //Post: 
    //Description: 
    public override void MidPowerAttack()
    {
        if (Input.GetButtonDown("Fire2") && player.playerClass is MarksmanClass && isHudOpen() && player.mana > basicManaCost && player.equippedWeapon != null)
        {
            cooldownTimer = 0f;

            firstPersonAnimation.SetBool("swing1", true);

            damage = player.strength + player.equippedWeapon.GetDamage();
            player.mana -= basicManaCost;

            GameObject projectile = Instantiate(prefabProjectile) as GameObject;
            projectile.transform.position = transform.position + Camera.main.transform.forward * 2;
            Rigidbody rb = projectile.GetComponent<Rigidbody>();
            rb.velocity = Camera.main.transform.forward * 40;

        }
    }

    //Pre: 
    //Post: 
    //Description: 
    public override void UltimateAttack()
    {

    }
}
