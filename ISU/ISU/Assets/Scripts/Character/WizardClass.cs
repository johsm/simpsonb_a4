﻿using UnityEngine;
using System.Collections;

public class WizardClass : PlayerClass {

    private RaycastHit closeProxyDamage;
    private Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
    private Vector3 attackDirection;

    GameObject prefabProjectile;

    public WizardClass()
    {
    }

    // Use this for initialization
    void Start () {
        prefabProjectile = Resources.Load("projectile") as GameObject;
        //combatControl = new CombatControls();

        basicCoolDown = 1f;
        midCoolDown = 30f;
        ultimateCoolDown = 10f;

        basicManaCost = 35f;
        midManaCost = 250f;
        ultimateManaCost = 0f;

        cooldownTimer = 0f;

        distance = 0f;
        damage = 0f;
    }
	
	// Update is called once per frame
	void Update () {
        Attacks();

    }

    //Pre: 
    //Post: 
    //Description: 
    public override void BaseAttack()
    {
        if (Input.GetButtonDown("Fire1") && player.playerClass is WizardClass && isHudOpen() && player.mana > basicManaCost && player.equippedWeapon != null)
        {
            cooldownTimer = 0f;

            damage = player.intelligence + player.equippedWeapon.GetDamage();
            player.mana -= basicManaCost;

            firstPersonAnimation.SetBool("swing1", true);

            GameObject projectile = Instantiate(prefabProjectile) as GameObject;
            projectile.transform.position = transform.position + Camera.main.transform.forward * 2;
            Rigidbody rb = projectile.GetComponent<Rigidbody>();
            rb.velocity = Camera.main.transform.forward * 40;

        }
    }

    //Pre: 
    //Post: 
    //Description: 
    public override void MidPowerAttack()
    {
        if (Input.GetButtonDown("Fire2") && player.playerClass is WizardClass && isHudOpen() && player.mana > midManaCost && player.equippedWeapon != null)
        {
            cooldownTimer = 0f;

            GameObject prefab = Resources.Load("lava") as GameObject;
            GameObject lava = Instantiate(prefab) as GameObject;
            lava.transform.position = transform.position;
            Destroy(lava, 13);

            damage = player.intelligence + player.equippedWeapon.GetDamage();
            player.mana -= midManaCost;
        }
    }

    //Pre: 
    //Post: 
    //Description: 
    public override void UltimateAttack()
    {
    
    }

}
