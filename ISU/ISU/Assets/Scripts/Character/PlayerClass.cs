﻿using UnityEngine;
using System.Collections;

public class PlayerClass : MonoBehaviour {

    //public CombatControls combatControl;
    public Character player;
    public UIController uiController;
    public GameObject target;

	public float damage;
    public float distance;

    public Animator firstPersonAnimation;

    protected float basicCoolDown;
    protected float midCoolDown;
    protected float ultimateCoolDown;

    protected float basicManaCost;
    protected float midManaCost;
    protected float ultimateManaCost;

    public float cooldownTimer;

    public PlayerClass()
    {
       // combatControl = new CombatControls();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //Pre: 
    //Post: 
    //Description: 
    public virtual void BaseAttack()
    {

    }

    //Pre: 
    //Post: 
    //Description: 
    public virtual void MidPowerAttack()
    {

    }

    //Pre: 
    //Post: 
    //Description: 
    public virtual void UltimateAttack()
    {

    }

    public bool isHudOpen()
    {
        if (uiController.gameState == uiController.PLAY)
        {
            return true;
        }

        return false;
    }

    public void Attacks()
    {
        cooldownTimer += Time.deltaTime;

        if (cooldownTimer >= basicCoolDown)
        {
            firstPersonAnimation.SetBool("swing1", false);
            BaseAttack();
        }
        if (cooldownTimer >= midCoolDown)
        {
            MidPowerAttack();
        }
        if (cooldownTimer >= ultimateCoolDown)
        {
            firstPersonAnimation.SetBool("swing2", false);
            UltimateAttack();
        }
    }
}
