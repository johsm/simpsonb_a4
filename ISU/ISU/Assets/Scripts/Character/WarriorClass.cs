﻿using UnityEngine;
using System.Collections;

public class WarriorClass : PlayerClass
{
	private RaycastHit closeProxyDamage;
	private Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
    private Vector3 attackDirection;

    public WarriorClass()
    {

    }

    // Use this for initialization
    void Start () {
        //combatControl = new CombatControls();

        basicCoolDown = 1f;
        midCoolDown = 1f;
        ultimateCoolDown = 5f;

        basicManaCost = 0f;
        midManaCost = 0f;
        ultimateManaCost = 0f;

        cooldownTimer = 0f;

        distance = 0f;
        damage = 0f;
	}
	
	// Update is called once per frame
	void Update () {

        Attacks();


    }

    //Pre: 
    //Post: 
    //Description: 
	public override void BaseAttack()
    {
        if (Input.GetButtonDown("Fire1") && player.playerClass is WarriorClass && isHudOpen() && player.equippedWeapon != null)
        {
            firstPersonAnimation.SetBool("swing1", true);
            cooldownTimer = 0f;

            damage = player.strength + player.equippedWeapon.GetDamage();
            player.mana -= basicManaCost;
            distance = Vector3.Distance(target.transform.position, transform.position);

            attackDirection = (target.transform.position - transform.position).normalized;

            //
            float direction = Vector3.Dot(attackDirection, transform.forward);

            Debug.Log(direction);

            if (distance < 2.5 && direction > 0)
            {
                Debug.Log("Hit: " + damage + "\nDistance: " + distance);
            }
            else
            {
                Debug.Log("No hit\nDistance: " + distance);
            }

        }
    }

    //Pre: 
    //Post: 
    //Description: 
    public override void MidPowerAttack()
    {
    }

    //Pre: 
    //Post: 
    //Description: 
    public override void UltimateAttack()
    {
        if (Input.GetButtonDown("Fire3") && isHudOpen() && player.playerClass is WarriorClass && player.mana >= ultimateManaCost && player.equippedWeapon != null)
        {
            GameObject fireWorkPrefab = Resources.Load("explosion") as GameObject;
            GameObject explosian = Instantiate(fireWorkPrefab) as GameObject;
            explosian.transform.position = transform.position;
            Destroy(explosian,5);


            firstPersonAnimation.SetBool("swing2", true);
            cooldownTimer = 0f;

            damage = (player.strength * 1.5f) + player.equippedWeapon.GetDamage();
            player.mana -= ultimateManaCost;

            distance = Vector3.Distance(target.transform.position, transform.position);

            if (distance < 10)
            {
                Debug.Log("Hit: " + damage + "\nDistance: " + distance);
            }
            else
            {
                Debug.Log("No hit\nDistance: " + distance);
            }

        }
    }
}
