﻿//Author Name: Ivgeni Darinski
//File Name: Character.cs
//Project Name: Polyrim
//Creation Date: 21/12/2015
//Modified Date: 1/5/2016
//Description: The character 
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using AssemblyCSharp;

public class Character : MonoBehaviour {

	GameObject pickClass;

	//The region that holds player constants
	#region Calculation Constants Variables

	//Stored as the default values for the players health and mana
	const double DEFAULT_HEALTH = 100;
	const double DEFAULT_MANA = 100;

	//Stored as the default values for the players regeneration rate for mana/health
	const double DEFAULT_HEALTH_REGEN = 1;
	const double DEFAULT_MANA_REGEN = 1;

    //Stored as the amount of attribute points a player gains every level.
    const int ATTRIBUTE_POINTS_PER_LEVEL = 2;

	#endregion

	//The region in which holds the players base information/stats.
	#region Character Information Variables

	//Players total progression in the game through killing and completing quests.
	public int level;
	public double experience;

	//Players attributes points that they can spend.
	public int attributePointsToSpend;

	//Players health and mana for getting hit or using an ability.
	public double health;
	public double mana;

	//Players max health and max mana.
	public double maxHealth;
	public double maxMana;

	//Players health regen rate and mana regen rate
	public double healthRegen;
	public double manaRegen;

    //The players health buff given by the companions
    public double healthBuff;
    public double healthRegenBuff;

    //The players mana buff given by the companions.
    public double manaBuff;
    public double manaRegenBuff;

    //Players ability scores to determine the hit strength, health, walkspeed, etc.
    public int strength;
	public int dexterity;
	public int intelligence;

	//Players reistance from enemy damage and magic attacks.
	public float defence;

	//The players class that they will play throughout the game
	//Remains constant throughout the game it is unchangeable.
	public PlayerClass playerClass;

	#endregion

	//The region that holds characters external attributes variable buffs, coins, etc.
	#region External Character Information Variables

    //An instance of the players inventory to store items.
	public Inventory inventory;

    //The models for each weapon tier that the player may equip.
	public GameObject[] swordModel = new GameObject[3];
	public GameObject[] staffModel = new GameObject[3];
	public GameObject[] daggerModel = new GameObject[3];
    public GameObject gun;


<<<<<<< HEAD
    //The players coins
	public int coins = 0;
=======
    //Players reistance from enemy damage and magic attacks.
    public float defence;
>>>>>>> 0c42ab679fd4c3c93194cd7e1e1284341e7c9f03

	#endregion

<<<<<<< HEAD
	//The region that holds the logic variables for updating character stats.
	#region Character Logic Variables

	//The time used to regenerate players mana and health.
	float regenerationTimer;

	Animator fistAnimations;
=======
    //The players class that they will play throughout the game
    //Remains constant throughout the game it is unchangeable.
    public PlayerClass playerClass;
>>>>>>> 0c42ab679fd4c3c93194cd7e1e1284341e7c9f03

    #endregion

    //The region that holds the logic variables for combat
    #region Character Combat Variables

    public Item equippedWeapon;
	public int tempTierLevel;

    public float helmetBuff = 0f;
    public float bodyBuff = 0f;
    public float feetBuff = 0f;

    public float damage = 0f;

	public int weaponIndex;

    #endregion

    //Set and Get subprograms for the characters information
    #region Set/Get Subprograms

    //Pre: An instance of a player class comes in.
    //Post: N/A
    //Description: Sets the players class in the beginning so they may use abilities and equip armour. 
    public void SetClass(PlayerClass playerClass)
	{
		this.playerClass = playerClass;
	}

	//Pre: N/A
	//Post: Returns the PlayerClass instance.
	//Description: Returns the players class for checking what equipment they may use and abilities.
	public PlayerClass GetClass()
	{
		return playerClass;
	}

	//Pre: N/A
	//Post: Returns the total number of armour defense.
	//Description: Adds up the total defense from each of the armour pieces for defense variable.
	public float GetDefence()
	{
		return helmetBuff + bodyBuff + feetBuff;
	}

	#endregion

	// Use this for initialization
	void Start () {

		//Sets default values for the players level and experience.
		level = 0;
		experience = 0;

		//Sets the default attributes for str, dex, and intelligence.
		strength = 1;
		dexterity = 1;
		intelligence = 1;

		playerClass = new WizardClass();
	}

	// Update is called once per frame
	void Update ()
<<<<<<< HEAD
	{
		//Increases time interval every second.
		regenerationTimer += Time.deltaTime;

		//Updates the players max health and health regen based on strength.
		maxHealth = strength * DEFAULT_HEALTH + healthBuff;
		healthRegen = strength * DEFAULT_HEALTH_REGEN;

		//Updates the players max mana and mana regen based on intelligence.
		maxMana = intelligence * DEFAULT_MANA + manaBuff;
		manaRegen = intelligence * DEFAULT_MANA_REGEN;

        //Updates the players defence for calculations used to calculate damage taken.
		defence = GetDefence();

        //Updates the players currently equipped weapon.
		equippedWeapon = inventory.equipment[3];

		//Checks if the player leveled up.
		if (experience >= NextLevelExperience(level))
		{
			//Levels up the player, sets the new attributes and sets the experience back to 0.
			level++;
			attributePointsToSpend += 2;
			experience = 0;

			//Sets the health and mana to their maximum values.
			health = maxHealth;
			mana = maxMana;
		}

		//Region that holds the health logic for regeneration and managment
		#region Health Managment/Regeneration

		//Checks if the timer has passed 1 second mark
		if (regenerationTimer >= 1)
		{
			//Checks if health is less than max health
			if (health < maxHealth)
			{
				health += healthRegen; //Regenerates the health
			}
			//Checks if mana is less than max mana
			if (mana < maxMana)
			{
				mana += manaRegen; //Regenerates the mana
			}
			//Sets the regeneration timer to 0.
			regenerationTimer = 0;
		}

		//Checks if health is greater than max health to set health to max health.
		if (health > maxHealth)
		{
			health = maxHealth;
		}
		//Checks if mana is greater than max mana to set health to max mana.
		if (mana > maxMana)
		{
			mana = maxMana;
		}

		#endregion

		WeaponEquipment();
		WeaponDequip (swordModel);
		WeaponDequip (staffModel);
		WeaponDequip (daggerModel);

    }

	public void WeaponEquipment()
	{
        //Checks if the weapon equipped is a sword, staff or gun.
        if (equippedWeapon is Sword || equippedWeapon is Staff || equippedWeapon is Gun)
		{
            //Checks if the weapon equipped is sword
            if (equippedWeapon is Sword)
=======
    {
        //Increases time interval every second.
        regenerationTimer += Time.deltaTime;

        //Updates the players max health and health regen based on strength.
        maxHealth = strength * DEFAULT_HEALTH + healthBuff;
        healthRegen = strength * DEFAULT_HEALTH_REGEN;

        //Updates the players max mana and mana regen based on intelligence.
        maxMana = intelligence * DEFAULT_MANA + manaBuff;
        manaRegen = intelligence * DEFAULT_MANA_REGEN;

        defence = GetDefence();

        //Checks if the player leveled up.
        if (experience >= NextLevelExperience(level))
        {
            //Levels up the player, sets the new attributes and sets the experience back to 0.
            level++;
            attributePointsToSpend += 2;
            experience = 0;

            //Sets the health and mana to their maximum values.
            health = maxHealth;
            mana = maxMana;
        }


        //Checks if the timer has passed 1 second mark
        if (regenerationTimer >= 1)
        {
            //Checks if health is less than max health
            if (health < maxHealth)
>>>>>>> 0c42ab679fd4c3c93194cd7e1e1284341e7c9f03
            {
                //Makes the sword model visible.
                WeaponActivation(swordModel, true);
            }
            //Checks if the weapon equipped is a staff
            else if (equippedWeapon is Staff)
            {
                //Makes the staff model visible.
                WeaponActivation(staffModel, true);
            }
            //Checks if the weapon equipped is a gun
            else if (equippedWeapon is Gun)
            {
                //Makes the dagger model and gun visible.
                WeaponActivation(daggerModel, true);
                gun.SetActive(true);
            }
        }
	}

	//Pre: The players current level comes in.
	//Post: The amount of experience needed to level up.
	//Description: Uses the formula of ( 50/3*(x^3 - 6*x^2 + 17*x - 12) ) where x is the level to determine the xp needed to level up.
	public double NextLevelExperience(int level)
	{
		return level*250;
	}
	//Pre: An array of weapons and a boolean that determines if the model is active.
	//Post: N/A
	//Description: Activates/deactives a certain weapon model depending on the tier level of the weapon equipped.
    public void WeaponActivation(GameObject[] weaponClass, bool isActive)
	{
		//Loops through the amount of tiers there are.
		for (int i = 1; i <= 3; i++) 
		{
			//Checks if the equipped weapon is equal to the current index of loop and set its to the current activation state from parameters.
			if (equippedWeapon.GetTierLevel () == i) 
			{
				weaponClass [i - 1].SetActive (isActive);
			} 
		}
	}

	//Pre: An array of weapons and a boolean that determines if the model is active.
	//Post: N/A
	//Description: Goes through the certain tiers of models of the weapons and removes them if they are not being used.
	public void WeaponDequip(GameObject[] weaponClass) {
		
		//Loops through the amount of tiers.
		for (int i = 0; i < 3; i++)
		{
			//Checks if the equipped weapon is equal to the current index of loop or if a current sword model is actived and if it doesn't match the current equipped weapons tier level
			if (inventory.equipment[3] == null || (weaponClass[i].activeInHierarchy == true && equippedWeapon.GetTierLevel() != i + 1) )
			{
				//Sets weapon activation state to false so it is dequipped.
				weaponClass [i].SetActive (false);
			}
		}

        //Checks if the equipped weapon is not a gun.
        if (equippedWeapon is Gun != true)
        {
            //Sets the visiblity of the weapon to false.
            gun.SetActive(false);
        }
<<<<<<< HEAD
	}
=======

    }

    //Pre: The players current level comes in.
    //Post: The amount of experience needed to level up.
    //Description: Uses the formula of ( 50/3*(x^3 - 6*x^2 + 17*x - 12) ) where x is the level to determine the xp needed to level up.
    public double NextLevelExperience(int level)
    {
        return level*250;
    }
>>>>>>> 0c42ab679fd4c3c93194cd7e1e1284341e7c9f03

}
