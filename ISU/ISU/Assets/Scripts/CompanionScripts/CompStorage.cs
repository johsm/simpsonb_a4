﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AssemblyCSharp
{
    class CompStorage : CompanionBase
    {
        private Inventory petInventory;
        private int playerLevel;

        public CompStorage()
        {
            petInventory = new Inventory();
            playerLevel = 1;
        }

        /// <summary>
        /// Retrieves the inventory of the companion
        /// </summary>
        /// <returns>Returns the companion inventory</returns>
        public override Inventory GetStorage()
        {
            return petInventory;
        }

        /// <summary>
        /// Retrieves the players current level
        /// </summary>
        /// <returns>Returns the player level</returns>
        public override int GetPlayerLevel()
        {
            return playerLevel;
        }

        /// <summary>
        /// Sets the players current level
        /// </summary>
        /// <param name="pLevel">Player level is passed</param>
        public override void SetPlayerLevel(int pLevel)
        {
            playerLevel = pLevel;
        }
    }
}
