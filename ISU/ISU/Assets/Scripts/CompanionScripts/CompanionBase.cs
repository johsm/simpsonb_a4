﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AssemblyCSharp
{
    class CompanionBase
    {
        public CompanionBase()
        {
        }

        public virtual int GetRecoverValue()
        {
            return 0;
        }

        public virtual Inventory GetStorage()
        {
            return null;
        }
        
        public virtual void SetPlayerLevel(int pLevel)
        {
            
        }

        public virtual int GetPlayerLevel()
        {
            return 0;
        }

    }
}
