﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AssemblyCSharp
{
    class CompHealthRec : CompanionBase
    {
        private int hpRegenValue;
        private int playerLevel;

        public CompHealthRec()
        {
            playerLevel = 1;
            hpRegenValue = 1;
        }

        /// <summary>
        /// Returns the regen value of the companion, buffed by the level of the player
        /// </summary>
        /// <returns>Returns the product of the regenvalue and the player level</returns>
        public override int GetRecoverValue()
        {
            return hpRegenValue * playerLevel;
        }

        /// <summary>
        /// Retrieves the players current level
        /// </summary>
        /// <returns>Returns the player level</returns>
        public override int GetPlayerLevel()
        {
            return playerLevel;
        }

        /// <summary>
        /// Sets the players current level
        /// </summary>
        /// <param name="pLevel">Player level is passed</param>
        public override void SetPlayerLevel(int pLevel)
        {
            playerLevel = pLevel;
        }
    }


}
